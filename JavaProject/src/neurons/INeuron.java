package neurons;

import exceptions.AiExceptionBackward;
import exceptions.AiExceptionForward;

public abstract class INeuron {
	public abstract double forward(double[] inputArr) throws AiExceptionForward;
	public abstract double[] backward(double inputDouble, double[] inputArr) throws AiExceptionBackward;
	public abstract int getWSize();
}