package neurons;

import exceptions.*;
import transfertFunctions.ITransfertFunction;
import utils.IInitialiseBias;
import utils.IInitialiseWeights;

public class NeuronLinear extends  INeuron {
	private double[] w;
	private double b;
	private double lr;
	private double[] xt;
	private double yt;
	private ITransfertFunction tf;
	
	private static ErrorLevel ERR_LVL = ErrorLevel.NEURON;
	
	public NeuronLinear(double lr, int inputSize, IInitialiseWeights initW, IInitialiseBias initB, ITransfertFunction tf) {
		this.w 		= 		initW.initWeights(inputSize);
		this.b 		= 		initB.initBias();
		this.lr 	= 		lr;
		this.xt 	= 		initW.initWeights(inputSize);
		this.yt 	= 		initB.initBias();
		this.tf 	= 		tf;
	}

	public double forward(double[] inputArr) throws AiExceptionForward {
		try {
			System.out.println("Neuron forward");
			this.xt 		= 		inputArr ;
			double output 	= 		0.0 ;
			System.out.println("For Forward {");
			for (int i = 0; i < (int)(inputArr.length) -1; i++) {
				System.out.println("\t" + inputArr[i]);
				output += this.w[i] * inputArr[i] ;
				this.yt = this.tf.ft(output + this.b);
			}
			System.out.println("}");
			return this.yt ;
		} catch(Exception e) {
			throw new AiExceptionForward(e.getMessage(), ERR_LVL);
		}
	}
	
	public double[] backward(double dy, double[] dxt) throws AiExceptionBackward {
		try {
			System.out.println("Neuron backward");
			double dxty = this.tf.dft(this.yt) * dy;
			for (int i = 0; i < (int)(xt.length) - 1; i++) {
				dxt[i] += this.w[i] * dxty ;
				this.w[i] += this.xt[i] * dxty * this.lr ;
			}
			this.b += dy * this.lr ;
			return dxt ;
		} catch(Exception e) {
			throw new AiExceptionBackward(e.getMessage(), ERR_LVL);
		}
	}

	public int getWSize() {
		return w.length ;
	}
}
