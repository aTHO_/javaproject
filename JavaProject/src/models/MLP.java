package models;

import exceptions.*;
import layers.LayerLinear;
import losses.LossDifference;
import transfertFunctions.*;
import utils.*;

public class MLP extends IModel{
	private static ErrorLevel ERR_LVL = ErrorLevel.MODEL;
	private 			LayerLinear[] layers ;
	private 			LossDifference lossFunction ;
	public double 		loss ;
	public double[]		input ;
	public double[] 	output ;
	public double[] 	predicted ;
	
	public MLP(int layerSize, ITransfertFunction tf) {
		this.lossFunction = new LossDifference();
		int currentSize = layerSize;
		int size = 0;
		while(currentSize != 1) {
			currentSize = (int) currentSize/2;
			size++;
		}
		this.layers = new LayerLinear[size+1];
		currentSize = layerSize*3;
		double lr = 0.0;
		InitialiseWeightsNormal initWeights =  new InitialiseWeightsNormal();
		InitialiseBiasNormal initBias = new InitialiseBiasNormal();
		this.layers[0] = new LayerLinear(currentSize, (int) currentSize-size/2, lr, initWeights, initBias, tf );
		int i = 0;
		while(currentSize != 1) {
			this.layers[i] = new LayerLinear(currentSize, (int) currentSize/2, lr, initWeights, initBias, tf );
			currentSize = (int) currentSize/2;
			i++;
		}
	}
	
	public double[] forward(double[] input) throws AiExceptionForward {
		try {
			double[] out = new double[input.length];
			//System.out.println("In forward");
			for (int i = 0; i < layers.length; i++) {
				//System.out.println("Layer number " + i + " :");
				out = layers[i].forward(input); //Appel la méthode forward de layer
			}
			return out ;
		} catch(Exception e) {
			throw new AiExceptionForward(e.getMessage(), ERR_LVL);
		}
	}

	public double backward(double[] output, double[] predicted) throws AiExceptionBackward, AiExceptionLoss {
		//try {
			double[] loss = this.lossFunction.loss(output, predicted) ;
			double[] dy = loss ;
			for (int i = layers.length -1 ; i >= 0; i -- )
				dy = layers[i].backward(dy); //Appel la méthode backward de layer
			return getLoss(loss) ;
		//} catch(Exception e) {
			//throw new AiExceptionBackward(e.getMessage(), ERR_LVL);
		//}
	}
	
	public static int getMaxIndice(double[] t) {
		int max = -1 ;
		double maxValue = -Double.MIN_VALUE ;
		for (int i = 0; i < t.length; i++) {
			if ( t[i] > maxValue ) {
				maxValue = t[i];
				max = i ;
			}
		}
		return max ;
	}
	
	public double getLoss(double[] dy) {
		double loss = 0.0 ;
		for (int i = 0; i < dy.length; i++)
			loss += dy[i];
		return loss /= dy.length ;
	}

	@Override
	public void learn() throws AiExceptionForward, AiExceptionBackward, AiExceptionLoss {
		System.out.print("this.input = [");
		for(int i =0; i < this.input.length; i++) {
			System.out.print(this.input[i] + ", ");
		}
		System.out.print("]\nthis.predicted = [");
		for(int i =0; i < this.predicted.length; i++) {
		System.out.print(this.predicted[i]);
		}
		System.out.print("]\n");
		this.output = this.forward(this.input);
		System.out.println("OUTPUT = [" + this.output[0] + "]");
		System.out.println("predicted = [" + this.predicted[0] + "]");
		this.loss = this.backward(this.output, this.predicted);		
	}
}
