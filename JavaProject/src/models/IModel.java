package models;

import exceptions.AiExceptionBackward;
import exceptions.AiExceptionForward;
import exceptions.AiExceptionLoss;

public abstract class IModel {
	public abstract double[] forward(double[] input) throws AiExceptionForward ;
	public abstract double backward(double[] output, double[] predicted) throws AiExceptionBackward, AiExceptionLoss ;
	public abstract void learn() throws AiExceptionForward, AiExceptionBackward, AiExceptionLoss ;
}
