/**
 * Class for Sigmoid transfer function.
 * 
 * @author Aubertin Emmanuel | github.com/Athomisos
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

package transfertFunctions;

public class TransfertFunctionSigmoid extends ITransfertFunction{
	
	/**
	 * Calcul de la fonction de transfert Sigmoid pour la phase avant.
	 * @param double inputValue
	 * @return double 
	 */
	public double ft(double v) {
		return (1 / (1 + Math.exp(-v))) ;
		}
	
	/**
	 * Calcul de la fonction de transfert Sigmoid pour la phase avant.
	 * @param double inputValue
	 * @return double 
	 */
	public double dft(double v) {
		return (v * ( 1 - v ) ) ;
		}
}
