/**
 * Class for Tanh transfer function.
 * 
 * @author Aubertin Emmanuel | github.com/Athomisos
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

package transfertFunctions;

public class TransfertFunctionTanh extends ITransfertFunction {
	
	/**
	 * Calcul de la fonction de transfert Tanh pour la phase avant.
	 * @param double inputValue
	 * @return double 
	 */
	public double ft(double v) {
		double e = Math.exp(v) ;
		double me = Math.exp(-v) ;
		return ( (e - me) / (e + me) ) ;
	}
	
	/**
	 * Calcul de la fonction de transfert Tanh pour la phase arriere.
	 * @param double inputValue
	 * @return double 
	 */
	public double dft(double v) {
		return ( 1 - v * v ) ;
	}
}
