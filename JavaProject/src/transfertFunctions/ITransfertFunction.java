/**
 * Mother class for all transfer function.
 * 
 * @author Aubertin Emmanuel | github.com/Athomisos
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

package transfertFunctions;

public abstract class ITransfertFunction {
	/**
	 * Calcul de la fonction de transfert pour la phase avant.
	 * @param double inputValue
	 * @return double 
	 */
	public abstract double ft(double inputValue);
	/**
	 * Calcul de la fonction de transfert pour la phase arriere.
	 * @param double inputValue
	 * @return double 
	 */
	public abstract double dft(double inputValue);
}
