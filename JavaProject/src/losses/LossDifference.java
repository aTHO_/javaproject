package losses;

import exceptions.*;

public class LossDifference extends ILoss {
	public double[] loss(double[] y, double[] yPredit) throws AiExceptionLoss {
		try { 
			int size = y.length;
			double[] output = new double[size];
			for(int i =0; i < size; i++) {
				output[i] = yPredit[i] - y[i];
			}
			return output;
		} catch(Exception e) {
			throw new AiExceptionLoss(e.getMessage(), ErrorLevel.LOSS);
		}
	}
}
