package losses;

import exceptions.AiExceptionLoss;

public abstract class ILoss {
	public abstract double[] loss(double[] y, double[] yPredit) throws AiExceptionLoss;
}
