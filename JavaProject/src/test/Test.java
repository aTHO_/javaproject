package test;

import exceptions.*;
import models.MLP;
import transfertFunctions.*;

public class Test {

	public static void main(String[] args) throws AiExceptionForward, AiExceptionBackward, AiExceptionLoss {
		// TODO Auto-generated method stub
		
		int LayerSize =  8 ; // First layer size

		double[] data = new double[3];
		
		double[] prediction = new double[1];
		
		TransfertFunctionSigmoid tf = new TransfertFunctionSigmoid();
		MLP myModel = new MLP(LayerSize, tf);
		
		for(int i = 0; i < 1000; i++) {
			data[0] = Math.round(Math.random() * ( 9 - 1 ));
			data[1] = 1; //operation +
			data[2] = Math.round(Math.random() * ( 9 - 1 ));
			prediction[0] = data[0] + data[2];
			
			myModel.input = data;
			myModel.predicted = prediction;
			myModel.learn();
			System.out.println("Output : " + myModel.output[0] + " | Predicted : " + myModel.predicted[0] + "\t| Accurate : " + (float)(myModel.loss/myModel.predicted[0])*100);
		}
		
		/*for(int i = 0; i < 5; i++)
		{
			double[] inputs = new double[]{, Math.round(Math.random()* ( 9 - 1 ))};
			double[] output = new double[1];
			double error;
			
			
			if((inputs[0] == inputs[1]) && (inputs[0] == 1))
				output[0] = 1.0;
			else
				output[0] = 0.0;
			

			System.out.println(inputs[0]+" and "+inputs[1]+" = "+output[0]);
			
			error = net.backPropagate(inputs, output);
			System.out.println("Error at step "+i+" is "+error);
		}*/
	}

}
