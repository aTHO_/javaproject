package layers;

import java.util.List;

import exceptions.AiExceptionBackward;
import exceptions.AiExceptionForward;
import neurons.INeuron;

public abstract class ILayer implements java.util.List {
	protected List<INeuron> neuronList;
	public abstract double[]  forward(double[] input) throws AiExceptionForward;
	public abstract double[]  backward(double[] dy) throws AiExceptionBackward;
}
