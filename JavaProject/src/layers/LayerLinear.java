package layers;

import java.util.*;

import exceptions.*;
import neurons.*;
import transfertFunctions.ITransfertFunction;
import utils.IInitialiseBias;
import utils.IInitialiseWeights;

public class LayerLinear extends ILayer{

	private static ErrorLevel ERR_LVL = ErrorLevel.LAYER; 

	public LayerLinear(int inputSize, int outputSize, double lr, IInitialiseWeights
	initWeights, IInitialiseBias initBias, ITransfertFunction tf) {
		this.neuronList = new ArrayList<INeuron>();
		for(int i=0; i < outputSize; i++) {
			this.add(new NeuronLinear(lr,(int) inputSize/outputSize, initWeights, initBias, tf));
		}
	}
	
	public double[] forward(double[] input) throws AiExceptionForward {
		try {
			double[] out = new double[this.size()];
			int pos = 0 ;
			for ( INeuron neuron : this.neuronList ) {//Boucle for à modifier …
				//System.out.println("neuron number " + pos + " :");
				out[pos++] = neuron.forward(input); //Appel la méthode forward de neurone
			}
			return out ;
		} catch(Exception e) {
			throw new AiExceptionForward(e.getMessage(), ERR_LVL);
		}
	}
	
	public double[] backward(double[] dy) throws AiExceptionBackward {
		try {
			int pos = 0 ;
			double[] dxt = new double[((INeuron) this.get(0)).getWSize()];
			for ( INeuron neuron : this.neuronList ) {
				dxt = neuron.backward(dy[pos++],dxt);//Appel la méthode backward de neurone
			}
			return dxt ;
		} catch(Exception e) {
			throw new AiExceptionBackward(e.getMessage(), ERR_LVL);
		}
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return this.neuronList.size();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return  this.neuronList.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return  this.neuronList.contains( (INeuron) o);
	}

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return  this.neuronList.iterator();
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return this.neuronList.toArray();
	}

	@Override
	public Object[] toArray(Object[] a) {
		// TODO Auto-generated method stub
		return this.neuronList.toArray( (INeuron[]) a);
	}

	@Override
	public boolean add(Object e) {
		// TODO Auto-generated method stub
		return this.neuronList.add((INeuron) e);
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return this.neuronList.remove((INeuron) o);
	}

	@Override
	public boolean containsAll(Collection c) {
		// TODO Auto-generated method stub
		return this.neuronList.containsAll(c);
	}

	@Override
	public boolean addAll(Collection c) {
		// TODO Auto-generated method stub
		return this.neuronList.addAll(c);
	}

	@Override
	public boolean addAll(int index, Collection c) {
		// TODO Auto-generated method stub
		return this.neuronList.addAll(index, c);
	}

	@Override
	public boolean removeAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection c) {
		// TODO Auto-generated method stub
		return this.neuronList.retainAll(c);
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		this.neuronList.clear();
	}

	@Override
	public Object get(int index) {
		// TODO Auto-generated method stub
		return this.neuronList.get(index);
	}

	@Override
	public Object set(int index, Object element) {
		// TODO Auto-generated method stub
		return this.neuronList.set(index, (INeuron) element);
	}

	@Override
	public void add(int index, Object element) {
		// TODO Auto-generated method stub
		this.neuronList.add(index, (INeuron) element);
	}

	@Override
	public Object remove(int index) {
		// TODO Auto-generated method stub
		return this.neuronList.remove(index);
	}

	@Override
	public int indexOf(Object o) {
		// TODO Auto-generated method stub
		return this.neuronList.indexOf(o);
	}

	@Override
	public int lastIndexOf(Object o) {
		// TODO Auto-generated method stub
		return this.neuronList.lastIndexOf(o);
	}

	@Override
	public ListIterator listIterator() {
		// TODO Auto-generated method stub
		return this.neuronList.listIterator();
	}

	@Override
	public ListIterator listIterator(int index) {
		// TODO Auto-generated method stub
		return this.neuronList.listIterator(index);
	}

	@Override
	public List subList(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return this.neuronList.subList(fromIndex, toIndex);
	}
}
