package utils;

public class InitialiseWeightsNormal extends IInitialiseWeights {
	public double[] initWeights(int size) {
		double[] ouputArr = new double[size];
		for(int i = 0; i < size; i++) {
			ouputArr[i] = Math.round(Math.random());
		}
		return ouputArr;
	}
}
