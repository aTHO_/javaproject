/**
 * Mother Class for initialise weights.
 * 
 * @author Aubertin Emmanuel | github.com/Athomisos
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

package utils;

public abstract class IInitialiseWeights {
	public IInitialiseWeights() {}
	public abstract double[] initWeights(int size);
}
