/**
 * Mother Class for initialise bias.
 * 
 * @author Aubertin Emmanuel | github.com/Athomisos
 * 
 * @copyright Aubertin Emmanuel
 * 
 * @version 1.0.0
 * 
 */

package utils;

public abstract class IInitialiseBias {
	public abstract double initBias();
}
