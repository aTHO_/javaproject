package exceptions;

public abstract class AiException extends Exception{
	public ErrorLevel errorLevel;
	public ErrorMethode errorMethode;
	
	public AiException(String message, ErrorLevel level, ErrorMethode methode) {
		super(methode + "(" + level + ") :" + message);
		this.errorMethode = methode;
		this.errorLevel = level;
	}
	
	public String getMessage() {
		return (this.errorMethode + "(" + this.errorLevel + ") :" + super.getMessage());
	}
}